package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ShoppingCart implements ShoppingCartOperation {

    private final List<ShoppingChatProduct> listOfProducts = new ArrayList<>();

    private Optional<ShoppingChatProduct> getProductWithNameFromList(String productName) {
        return listOfProducts
                .stream()
                .filter(x -> x.name.equals(productName))
                .findFirst();
    }


    public boolean addProducts(String productName, int price, int quantity) {
        Optional<ShoppingChatProduct> productWithName = getProductWithNameFromList(productName);

        if(!productWithName.isPresent()) {
            if(quantity<1 || price<0 || listOfProducts.size() == ShoppingCartOperation.PRODUCTS_LIMIT) {
                return false;
            }
            ShoppingChatProduct newProduct = new ShoppingChatProduct(productName, quantity, price);
            listOfProducts.add(newProduct);
            return true;
        }

        ShoppingChatProduct product = productWithName.get();
        if(product.price == price) {
            product.quantity += quantity;
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        Optional<ShoppingChatProduct> productWithName = getProductWithNameFromList(productName);
        if(!productWithName.isPresent()) {
            return false;
        }
        ShoppingChatProduct product = productWithName.get();
        if(product.quantity >= quantity && quantity >= 0) {
            int newQuantity = product.quantity - quantity;
            productWithName.get().setQuantity(newQuantity);
            return true;
        }
        return false;
    }

    public int getQuantityOfProduct(String productName) {
        Optional<ShoppingChatProduct> productWithName = getProductWithNameFromList(productName);
        return productWithName.map(shoppingChatProduct -> shoppingChatProduct.quantity).orElse(0);
    }

    public int getSumProductsPrices() {
        int sum = 0;
        for(ShoppingChatProduct product : listOfProducts) {
            sum += product.price;
        }
        return sum;
    }

    public int getProductPrice(String productName) {
        Optional<ShoppingChatProduct> productWithName = getProductWithNameFromList(productName);
        return productWithName.map(shoppingChatProduct -> shoppingChatProduct.price).orElse(0);
    }

    public List<String> getProductsNames() {
        List<String> names = new ArrayList<>();
        for(ShoppingChatProduct product : listOfProducts) {
            names.add(product.name);
        }
        return names;
    }
}
